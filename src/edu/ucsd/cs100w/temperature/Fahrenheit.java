/**
 * 
 */
package edu.ucsd.cs100w.temperature;

/**
 * @author lishih
 *
 */
public class Fahrenheit extends Temperature{
	float val2;
	public Fahrenheit(float t) {
		super(t);
		val2 = t;
	}
	public String toString() {
		return ""+  val2 + " F";
	}
	@Override
	public Temperature toCelsius() {
		float new_val = (float) (((val2-32)*5)/9);
		Temperature temp = new Celsius(new_val);
		return temp;
	}
	@Override
	public Temperature toFahrenheit() {
		Temperature temp = new Fahrenheit(val2);
		return temp;
	}
	@Override
	public Temperature toKelvin() {
		float new_val = (float) (((val2-32)*5)/9);
		new_val = (float)(new_val + 273.15);
		Temperature temp = new Kelvin(new_val);
		return temp;
	}
}
