/**
 * 
 */
package edu.ucsd.cs100w.temperature;

/**
 * @author lishih
 *
 */
public class Celsius extends Temperature{
	float val;
	public Celsius(float t) {
		super(t);
		val = t;
	}
	public String toString() {
		return ""+  val + " C";
	}
	@Override
	public Temperature toCelsius() {
		Temperature temp = new Celsius(val);
		return temp;
	}
	@Override
	public Temperature toFahrenheit() {
		float new_val = (float) ((1.8 * val) + 32);
		Temperature temp = new Fahrenheit(new_val);
		return temp;
	}
	@Override
	public Temperature toKelvin() {
		float new_val = (float)(val+273.15);
		Temperature temp = new Kelvin(new_val);
		return temp;
	}
}
