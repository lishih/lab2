/**
 * 
 */
package edu.ucsd.cs100w.temperature;

/**
 * @author lishih
 *
 */
public abstract class Temperature {
	private float value;
	public Temperature(float v) {
		value = v;
	}
	public final float getValue() {
		return value;
	}
	public abstract Temperature toCelsius();
	public abstract Temperature toFahrenheit();
	public abstract Temperature toKelvin();
}
